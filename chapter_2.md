# Security Mechanisms

- Hash functions
- Digital Signatures

# Hash Functions

## Data Integrity

Protect against modification of data.

Compute a hash `h(x)` and store it unmodifiable. Compare for validation.
If hash is modifiable it can be tricked of course.

### One-Way Functions (Without Keys)

- **Ease of computation**
- **Compression**
- **Pre-image Resistance**: it is computationally infeasible to find an `x` for a
  given `y` with `h(x)=y`
- **2nd Pre-image Resistance**: it is computationally infeasible to find an `x'`
  for a given `x` with `x != x'` but `h(x) = h(x')`

For Collision Resistant Hash Functions in addition:

- **Collision Resistance** (strong collision resistance): it is computationally
  infeasible to find any two inputs `x` and `x'` with `x != x'` and `h(x) = h(x')`.

![Visualization](vis.png)

#### Discrete Exponentiation

`h(x) := g^x mod p`

**Discrete logarithm problem**: find `x` for `h(x)`. Difficult to solve for
*judicious choices* of `p` and `g`.

It is:

- A one way function
- Too slow for many applications

#### Traditional Construction

Designing fast hash functions:

- Let `f` be a *compression function* that works on fixed size input blocks
- Break up `x` into blocks, pad last block
- `h_i = f(x_i||h_(i-1))` with `||` doing concatenation

![Visualization](constr.png)

#### Hash Collision

Expected number of tries for finding an `x` with `h(x)=y` is `2^n` with `n`
being the bit-length.

A set with `2^(n/2)` hashes is likely to contain a collision.

#### Frequently Used Algorithms

- **MD5**, 128 bit, weak, no longer recommended
- **SHA-1**, 160 bit
- **RIPEMD-160**
- **SHA-256**, 256 bit
- **SHA-3**, 224 to 512 bit

### Key Based Hashes (Auth codes, signatures)

In communications, we cannot rely on secure storage. Let's use secrets!

Message Authentication Codes to the rescue: let's make it a `h_k(x)` with a
secret `k`. Sender and receiver share it.

A MAC needs:

- **Compression**
- **Ease of computation**
- **Computation resistance**: For a fixed unknown `k` but known `x_i` and
  `h_k(x_i)` it is computationally infeasible to compute `h_k(x)` for a new
  `x`.

#### Construction

HMAC:

- Take an algorithm `h`.
- For any `k` and message `x` let `HMAC(x) = h(k||p_1||h(k||p_2||x))` with
  `p_1` being another byte padding than `p_2`.

### Terminology

Result of a hash is called **hash value**, **message digest** or **checksum**.

CRC is not made for security.

## Digital Signatures

- A person holding the *private key* can *create* a signature.
- A person holding the *public key* can *verify* a signature.
- Assumption: my device already holds valid public keys.

It should be computationally infeasible to get a private from a public key.

### RSA Signatures

- Primes `op` and `q`. Should be large, else its easy to find.
- `m` be the message
- `n = p*g`
- `e` be a public exponent
- `d` private exponent

Send: `(m, y)` where `y = m^d mod n`
Verify: `y^e == m` is the validity of the signature

Public: e, n, m, y (enemy!)
Private: p, q, d

TODO: I think we need more info on this for the exam.

### Digital Signature Algorithm


